#include "QuickSort.h"
#include <random>
#include <array>
#include <algorithm>
#include <iostream>
#include <cassert>

using namespace std;

template <typename T, size_t S>
std::ostream &operator<< (std::ostream &os, std::array<T, S> const &array)
{
	os << "{array: ";
	for (auto &element : array)
	{
		os << element;
		if (element != array.back())
			os << ", ";
	}
	os << " }";
	return os;
}

template <typename T = int, size_t S = 20>
auto randomData() -> std::array<T, S>
{
	// fill array with random elements
	random_device rd;
	uniform_int_distribution dist(0, 99);

	array<T, S> arr;
	generate(arr.begin(), arr.end(), [&rd, &dist] { return dist(rd); });

	return arr;
}

void testPartition()
{
	auto arr = randomData<int, size_t(20)>();

	auto last_idx = arr.size() - 1;
	cerr << "before partition\n";
	cout << arr << endl;

	auto middle = ::partition(begin(arr), next(begin(arr), last_idx));
	auto middle_idx = middle - begin(arr);
	cerr << "middle idx " << middle_idx << endl;
	// check partition
	for (int i = 0; i < middle_idx; ++i)
	{
		assert(arr[i] < arr[middle_idx]);
	}
	for (int i = (int) last_idx; i > middle_idx; --i)
	{
		assert(!(arr[i] < arr[middle_idx]));
	}

	cout << arr << endl;
}
#include <list>
void testSort()
{
	//std::list<int> l = { 1,2,3 };
	// FAILS! expect random access iterator
	//quickSort(begin(l), end(l));

	auto arr = randomData();
	// make a copy to compare later
	auto arr2 = arr;
	quickSort(begin(arr), end(arr));
	cout << "Before sort\n\t" << arr2 << endl;
	cout <<"After sort\n\t"<< arr << endl;

	std::sort(begin(arr2), end(arr2));
	assert(arr == arr2);
}

using namespace std;
int main()
{
	//testPartition();
	testSort();

	
}