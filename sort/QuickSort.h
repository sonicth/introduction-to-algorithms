#pragma once 
//#include <iterator>
#include <utility>

#define _ITERATOR_DEBUG_LEVEL 0

template <typename Titer>
decltype(auto) partition(Titer first, Titer last)
{
	auto &x = *last;
	auto ij = first;
	//STL complains: auto ii = std::prev(first);
	//auto ii = ij; --ii;
	auto ii = ij; std::advance(ii, -1);

	while (ij < last)
	{
		if ((*ij < x))
		{
			++ii;
			std::swap(*ii, *ij);
		}
		++ij;
	}
	++ii;
	std::swap(*ii, *last);

	return ii;

}
#include <cassert>
template <typename Titer>
void quickSort(Titer first, Titer end)
{
	// check that iterator is random
	using category = std::iterator_traits<Titer>::iterator_category;
	static_assert(std::is_same_v <category, std::random_access_iterator_tag>);

	if (end - first <= 1)
		return;

	auto middle = ::partition(first, prev(end));
	quickSort(first, middle);
	quickSort(next(middle), end);
}

