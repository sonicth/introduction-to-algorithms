#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <random>

using namespace std;

template <typename T>
void merge1(vector<T> const &v1, vector<T> const &v2, vector<T> &out)
{
	auto i1 = begin(v1),
		i2 = begin(v2),
		e1 = end(v1), 
		e2 = end(v2);

	// clear result and insert to the back
	out.clear();
	out.reserve(v1.size() + v2.size());
	auto r = back_inserter(out);

	// merge ranges until all iterators are at the end
	while (i1 != e1 || i2 != e2)
	{
		if (i1 == e1)
			// first range is 
			*(r++) = *(i2++);
		else if (i2 == e2)
			*(r++) = *(i1++);
		else if (*i1 < *i2)
			*(r++) = *(i1++);
		else
			*(r++) = *(i2++);
	}
}

template <typename T>
ostream& operator<<(ostream& os, vector<T> const &xs)
{
	os << "{";
	auto& tail = xs.back();
	for (auto &x: xs)
	{
		os << x << (tail == x ? "" : ", ") ;
	}
	os << "}";
	return os;
}

template <typename T>
vector<T> mergeSort(vector<T> const& vi)
{
	if (vi.empty())
		throw std::runtime_error("empty vector");

	auto n = vi.size();

	// if it is a single element, return it
	if (n == 1)
		return vi;

	auto n_half = n / 2;

	// split into two sets against the mid
	auto b = begin(vi);
	auto e = end(vi);
	auto mid = next(b, n_half);
	vector<T> v1(b, mid);
	vector<T> v2(mid, e);

	// sort two parts recursively, and then merge them
	auto&& v1s = mergeSort(v1);
	auto&& v2s = mergeSort(v2);
	vector<T> vout;
	merge1(v1s, v2s, vout);
	return vout;
}


using V = vector<int>;

void testMerge()
{
	V v1 = { 1,3,5,7,9,101,102,103,104 };
	V v2 = { 2,4,6,8,10,12,14 };

	cout << "set1 " << v1 << endl;
	cout << "set2 " << v2 << endl;
	V vmerged;
	merge1(v1, v2, vmerged);
	cout << "merged " << vmerged << endl;
}

void testMergeSort()
{
	V vinput(21);
	iota(begin(vinput), end(vinput), 0);

	cout << "unpermuted " << vinput << endl;

	// shuffle
	std::random_device rd;
	std::mt19937 g(rd());
	//DOES NOT WORK IN vs! next_permutation(begin(vinput), end(vinput));
	shuffle(begin(vinput), end(vinput), g);
	cout << "permutation " << vinput << endl;

	// not sort
	cout << "sorting..." << endl;
	auto vsorted = mergeSort(vinput);
	cout << "sorted " << vsorted << endl;

}




int main()
{
	//testMerge();
	testMergeSort();

    return 0;
}
