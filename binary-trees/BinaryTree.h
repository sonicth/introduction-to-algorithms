#pragma once

#include <memory>
#include <functional> // for std::function ...
#include <iostream>
#include <cassert>

namespace MB
{
	template <typename T, typename Tnode>
	class BinaryTree;

	template <typename T>
	class TreeNode
	{
	public:
		using Ptr = std::unique_ptr<TreeNode<T>>;
		using value_type = T;
		using Data = T;


		TreeNode(T &&data_)
			: data{ data_ } {}

		TreeNode(T const &data_)
			: data{ data_ } {}

		TreeNode * getWest() { return west.get(); }
		TreeNode * getEast() { return east.get(); }
		TreeNode const *getP() const { return p; }
		TreeNode const * getWest() const { return west.get(); }
		TreeNode const * getEast() const { return east.get(); }
		Ptr cutWest() { return std::move(west); }
		Ptr cutEast() { return std::move(east); }

		TreeNode * setWest(Ptr n) { west = std::move(n); return getWest(); }
		TreeNode * setEast(Ptr n) { east = std::move(n); return getEast(); }

		T const& getData() const { return data; }
		void setParent(TreeNode *p_) { p = p_; }
	private:
		T data;
		// non-owning pointer to parent; default to nullptr
		TreeNode *p = nullptr;

		// owning pointers to children
		Ptr west, east;

		friend class BinaryTree<T, TreeNode<T>>;
	};

	template <typename T>
	T const& getData(TreeNode<T> const *n)
	{
		return n->getData();
	}

	template <typename T>
	TreeNode<T> * setWest(TreeNode<T> *n, typename TreeNode<T>::Ptr &&newn)
	{
		return n->setWest(std::move(newn));
	}

	template <typename T>
	TreeNode<T>* setEast(TreeNode<T>* n, typename TreeNode<T>::Ptr &&newn)
	{
		return n->setEast(std::move(newn));
	}


	template <typename T>
	TreeNode<T> const* getWest(TreeNode<T> const* n)
	{
		return n->getWest();
	}

	template <typename T>
	TreeNode<T> const* getEast(TreeNode<T> const* n)
	{
		return n->getEast();
	}

	template <typename T>
	TreeNode<T>* getWest(TreeNode<T>* n)
	{
		return n->getWest();
	}

	template <typename T>
	TreeNode<T>* getEast(TreeNode<T>* n)
	{
		return n->getEast();
	}



	template <typename T, typename Tnode = TreeNode<T>>
	class BinaryTree
	{
		using Data_t = T;
	public:
		using Node = Tnode;
		using NodePtr = typename Tnode::Ptr;

	protected:
		std::unique_ptr<Node> _root;

	private:
		template <typename TreduceFun>
		static size_t _countReduce(Node const * subtree, TreduceFun const &reduce)
		{
			if (subtree == nullptr)
				return 0;
			else
				return 1 + reduce(_countReduce(getWest(subtree), reduce), 
								  _countReduce(getEast(subtree), reduce));
		}

		static void _copy(Node const * src, NodePtr &dst)
		{
			if (src == nullptr)
				return;
			else
			{
				// data
				dst = std::make_unique<Node>(src->data);

				// children; recursively
				_copy(src->getWest(), dst->west);
				_copy(src->getEast(), dst->east);

				// parent; now that subtree has been created, reverse back setting parent pointers
				if (dst->getWest()) dst->getWest()->setParent(dst.get());
				if (dst->getEast())	dst->getEast()->setParent(dst.get());
			}
		}

		static void _insert(Node *subtree, typename Node::Data&& value)
		{
			assert(subtree != nullptr);

			if (value < getData(subtree))
			{
				if (getWest(subtree) != nullptr)
				{
					_insert(getWest(subtree), std::forward<Node::Data&&>(value));
				}
				else
				{
					setWest(subtree, std::make_unique<Node>(std::forward<Node::Data&&>(value)));
					getWest(subtree)->setParent(subtree);
				}
			}
			else
			{
				if (getEast(subtree) != nullptr)
				{
					_insert(getEast(subtree), std::forward<Node::Data&&>(value));
				}
				else
				{
					setEast(subtree, std::make_unique<Node>(std::forward<Node::Data&&>(value)));
					getEast(subtree)->setParent(subtree);
				}
			}
		}

		static Node const * _find(Node const* subtree, T const& value)
		{
			if (subtree == nullptr || subtree->getData() == value)
				return subtree;

			if (value < subtree->getData())
				return _find(getWest(subtree), value);
			else
				return _find(getEast(subtree), value);
		}

		static Node * _findIterative(Node * subtree, T const& value)
		{
			auto x = subtree;
			while (x != nullptr && x->getData() != value)
			{
				if (value < x->getData())
					x = x->getWest();
				else
					x = x->getEast();
			}

			return x;
		}

	public:
		template <typename Tnode>
		static void walk(Tnode const *subtree, std::function<void(typename Tnode::Data const &)> const &fun)
		{
			if (subtree != nullptr)
			{
				walk(getWest(subtree), fun);
				fun(subtree->data);
				walk(getEast(subtree), fun);
			}
		}

	public:
		bool operator== (BinaryTree<T> const &other) const
		{
			return equal(getRoot(), other.getRoot());
		}

		bool operator!= (BinaryTree<T> const &other) const
		{
			return !(*this == other);
		}

		size_t size() const noexcept 
		{
			auto count_nodes_f = [](auto r1, auto r2) { return r1 + r2; };
			return _countReduce(_root.get(), count_nodes_f);
		}

		size_t height() const noexcept
		{
			auto count_height_f = [](auto r1, auto r2) { return std::max(r1, r2); };
			return _countReduce(_root.get(), count_height_f);
		}

		void insert(typename Node::Data &&value)
		{
			if (_root == nullptr)
			{
				// insert new
				_root = std::make_unique<Node>(std::forward<Node::Data&&>(value));
			}
			else
			{
				_insert(_root.get(), std::forward<Node::Data&&>(value));
			}
		}

		typename TreeNode<T>::Ptr 
			delete_(Node *z)
		{
			Ptr y;
			auto y = (z->west == nullptr || z->east == nullptr)
				? z
				: successor(z);

			assert(y != nullptr);
			TreeNode<T>::Ptr x;
			if (y->west != nullptr)
			{
				x = std::move(y->west);
			}
			else 
			{
				x = std::move(y->east);
			}

			if (x != nullptr)
				x->p = y->p;


			if (y->p == nullptr) [[unlikely]]
			{
				// dont' destory the pointer!
				_root.release();
				// set new pointer to x
				_root = std::move(x);
			}
			else [[likely]]
			{
				//TODO remove smart pointers!!!
				if (y == getWest(y->p))
				{
					y->p->west.release();
					y->p->west = std::move(x);
				}
				else
				{
					y->p->east.release();
					y->p->east = std::move(x);

				}
			}

			if (y != z)
			{
				z->data = y->data;
			}

			return TreeNode<T>::Ptr(y);			
		}

		using const_iterator = Node const*;
		const_iterator find(T const& value)
		{
			return _find(getRoot(), value);
		}
		Node * findIter(T const& value)
		{
			return _findIterative(getRoot(), value);
		}

		virtual ~BinaryTree() = default;

		BinaryTree() = default;
		BinaryTree(NodePtr &&root_) : root{ std::move(root_) }
		{}
		BinaryTree(BinaryTree<T> const &other)
		{
			throw std::logic_error("not implemented");
		}

		BinaryTree & operator= (BinaryTree<T> const &other)
		{
			_copy(other.getRoot(), _root);
			return *this;
		}

		static Node *treeMin(Node *x)
		{
			if (x == nullptr)
				return x;

			while (getWest(x) != nullptr)
			{
				x = getWest(x);
			}
			
			return x;
		}

		static Node *treeMax(Node *x)
		{
			if (x == nullptr)
				return x;

			while (getEast(x) != nullptr)
			{
				x = getEast(x);
			}

			return x;
		}

		static Node * successor(Node *x)
		{
			assert(x != nullptr);
			if (x == nullptr) throw std::logic_error("did not expect null!");

			if (getEast(x) != nullptr)
				return treeMin(getEast(x));
			else
			{
				auto y = x->p;
				while (y != nullptr && x == getEast(y))
				{
					x = y;
					y = y->p;
				}

				return y;
			}
		}


		Node * const getRoot() const { return _root.get(); }
		Node * getRoot() { return _root.get(); }
		template <typename Tptr>
		void setRoot(Tptr &&ptr) { _root = std::forward<Tptr &&>(ptr); }

		void printData() const
		{
			walk(getRoot().get(), [](auto data) { std::cout << "\"" << data << "\" "; });
			std::cout << std::endl;
		}

	

	};

	inline
	void s(int state)
	{
		//std::cout << "*state (" << state << ")" << std::endl;
	}

	template <typename Tnode>
	bool equal(Tnode const *a, Tnode const *b)
	{
		// either of both reached the end
		if (a == nullptr || b == nullptr)
		{
			return (a == nullptr && b == nullptr) ? true : false;
		}

		// data and subtrees must be equal (those can be null, see above)
		return getData(a) == getData(b)
			&& equal(getWest(a), getWest(b))
			&& equal(getEast(a), getEast(b));
	}

	template <typename T, typename Fvisitor>
	void walkIteratively(BinaryTree<T> const& tree, Fvisitor& visitor)
	{
		typename BinaryTree<T>::Node const
			*xl = nullptr,
			*x = tree.getRoot();
		
		assert(x != nullptr);

		bool done = false;
		while (!done)
		{
			auto p = x->getP();
			auto w = x->getWest();
			auto e = x->getEast();
			
			if (xl == e && p == nullptr)
				// returned to the root from east branch; terminate!
			{
				s(-1);
				done = true;
			}
			else if (xl == w && p == nullptr)
				//  when returning from west branch && at root
			{
				s(1);
				visitor(x);
				xl = x;
				x = e;
			}
			else if (w != nullptr && xl == p)
				// first visitation of the node, go west
			{
				s(2);
				xl = x;
				x = w;
			}
			else if (e != nullptr &&
				(xl == w || xl == p))
				// visit current, time go east next
			{
				s(3);
				visitor(x);
				xl = x;
				x = e;
			}
			else if (e == nullptr &&
				(xl == w || xl == p))
				// cannot go east, visit and go to parent
				// NOTE very similar to the previous condition
			{
				s(4);
				visitor(x);
				xl = x;
				x = p;
			}
			else if (xl == e)
				// just came from east; go to parent
			{
				s(5);
				assert(e != nullptr);
				xl = x;
				x = p;
			}
			else
			{
				assert(!"something gone wrong!");
				throw std::logic_error("woops!");
			}

		}
	}


}
