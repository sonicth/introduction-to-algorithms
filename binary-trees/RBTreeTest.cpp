#include "BinaryTree.h"
#include "BinaryTreeUtils.h"

#include <boost/test/unit_test.hpp>

using namespace std;
namespace bu = boost::unit_test;
using namespace MB;


struct RBTreeParams
{
	// arguments
	int ac;
	char **av;

	RBTreeParams()
		: ac{ bu::framework::master_test_suite().argc }
		, av{ bu::framework::master_test_suite().argv }
		, t1{ createTestTree1_() }
		, t2{
			/// Textbook Figure 13.4 tree before insertion
			[]()
			{
				auto trb = make_unique<MB::RBTree<int>>();
				// root
				trb->insertViolating({11, RBColour::Black});
				// right
				trb->insertViolating({14, RBColour::Black});
				trb->insertViolating({15, RBColour::Red});
				// left
				trb->insertViolating({2, RBColour::Red});
				trb->insertViolating({1, RBColour::Black});
				trb->insertViolating({7, RBColour::Black});
				trb->insertViolating({8, RBColour::Red});
				trb->insertViolating({5, RBColour::Red});
				return trb;
			}() }
		, t2a{
			/// Textbook Figure 13.4 inserted before fixup (unbalanced)
			[]()
			{
				auto trb = make_unique<MB::RBTree<int>>();
				// root
				trb->insertViolating({11, RBColour::Black});
				// right
				trb->insertViolating({14, RBColour::Black});
				trb->insertViolating({15, RBColour::Red});
				// left
				trb->insertViolating({2, RBColour::Red});
				trb->insertViolating({1, RBColour::Black});
				trb->insertViolating({7, RBColour::Black});
				trb->insertViolating({8, RBColour::Red});
				trb->insertViolating({5, RBColour::Red});
				// insert another element (4)
				trb->insertViolating({ 4, RBColour::Red });
				return trb;
			}() }
		, t2d{
			/// Textbook Figure 13.4d inserted after fixup (balanced)
			[]()
			{
				auto trb = make_unique<MB::RBTree<int>>();
				// root
				trb->insertViolating({7, RBColour::Black});
				// right
				trb->insertViolating({11, RBColour::Red });
				trb->insertViolating({14, RBColour::Black});
				trb->insertViolating({15, RBColour::Red});
				trb->insertViolating({8, RBColour::Black});
				// left
				trb->insertViolating({2, RBColour::Red});
				trb->insertViolating({1, RBColour::Black});
				trb->insertViolating({5, RBColour::Black });
				trb->insertViolating({4, RBColour::Red});
				return trb;
			}() }
		, t2_eastInsert {
			/// Textbook Figure 13.4 inserted before fixup (unbalanced)
			[]()
			{
				auto trb = make_unique<MB::RBTree<int>>();
				// root
				trb->insertViolating({7, RBColour::Black});
				// right
				trb->insertViolating({11, RBColour::Red });
				trb->insertViolating({14, RBColour::Black});
				trb->insertViolating({15, RBColour::Red});
				trb->insertViolating({8, RBColour::Black});
				trb->insertViolating({9, RBColour::Red });
				// left
				trb->insertViolating({2, RBColour::Red});
				trb->insertViolating({1, RBColour::Black});
				trb->insertViolating({5, RBColour::Black });

				return trb;
			}() }
	{
	}

	std::unique_ptr<RBTree<int>> t1;
	std::unique_ptr<RBTree<int>> t2,t2a, t2d;
	std::unique_ptr<RBTree<int>> t2_eastInsert;

	std::unique_ptr<RBTree<int>> createTestTree1_()
	{

		auto trb = make_unique<MB::RBTree<int>>();
		trb->insertViolating({ 10, RBColour::Red });
		trb->insertViolating({ 7, RBColour::Black });
		trb->insertViolating({ 12, RBColour::Black });
		trb->insertViolating({ 11, RBColour::Red });
		trb->insertViolating({ 555, RBColour::Red });
		trb->insertViolating({ 5, RBColour::Black });
		trb->insertViolating({ 8, RBColour::Red });
		trb->insertViolating({ 3, RBColour::Black });

		return trb;
	}
};

template <typename T>
using Stream_t = typename MB::StreamTraits<int, RBNode<T>>::Stream;

BOOST_FIXTURE_TEST_CASE(RBTreeCreate, RBTreeParams)
{

	BOOST_TEST(t1.get() != nullptr);
	{
		Stream_t<int> os("rb-tree1.txt");
		os << *t1;
	}

	BOOST_TEST(t2.get() != nullptr);
	BOOST_TEST(t2a.get() != nullptr);
	BOOST_TEST(t2d.get() != nullptr);
	{ Stream_t<int> os("rb-tree2.txt");  os << *t2; }
	{ Stream_t<int> os("rb-tree2a.txt"); os << *t2a; }
	{ Stream_t<int> os("rb-tree2d.txt"); os << *t2d; }
}

BOOST_FIXTURE_TEST_CASE(RBTreeInsertViolating, RBTreeParams)
{
	t2->insertViolating({ 4, RBColour::Red });
	{ Stream_t<int> os("rb-tree2a.txt"); os << *t2; }

	BOOST_TEST((*t2) == (*t2a));
}

BOOST_FIXTURE_TEST_CASE(RBTreeInsertBalancedWest, RBTreeParams)
{
	t2->insert({ 4, RBColour::Red });
	{ Stream_t<int> os("rb-tree2insert.txt"); os << *t2; }

	BOOST_TEST((*t2) == (*t2d));
}

BOOST_FIXTURE_TEST_CASE(RBTreeInsertBalancedEast, RBTreeParams)
{
	t2->insert({ 9, RBColour::Red });
	{ Stream_t<int> os("rb-tree2insert-east.txt"); os << *t2; }

	BOOST_TEST((*t2) == (*t2_eastInsert));
}
