#pragma once

#include "BinaryTree.h"
#include "RedBlackTree.h"
#include <string>
#include <fstream>


namespace MB
{


	template <typename Tnode>
	void displayNode(std::ostream& os, Tnode const* node);

	template <class _Elem, class _Traits>
	class BasicBinaryTreeVizStream : public std::basic_ofstream<_Elem, _Traits>
	{
		int null_node_counter = 0;
	public:
		template <typename T>
		BasicBinaryTreeVizStream(T const& a)
			: std::basic_ofstream<_Elem, _Traits>(a)
		{}

		template <typename Tptr>
		void outDirection(Tptr source, Tptr target)
		{
			assert(source != nullptr);

			(*this) << source->getData() << " [color=gray style=filled]" << std::endl;

			if (target != nullptr)
			{
				(*this) << source->getData() << " -> " << target->getData() << ";" << std::endl;
			}
			else
			{
				// get unique null node id
				auto id = null_node_counter++;

				// hide duplicate null node
				(*this) << "null_" << id << " [label = \"null\" fillcolor=gray50 shape=box style=filled width=0.3 height=0.16 fontsize=9]" << std::endl;

				(*this) << source->getData() << " -> " << "null_" << id << std::endl;
			}
		}
	};

	//TODO merge with the above
	template <class _Elem, class _Traits>
	class BasicRBTreeVizStream : public std::basic_ofstream<_Elem, _Traits>
	{
		int null_node_counter = 0;
	public:
		template <typename T>
		BasicRBTreeVizStream(T const& a)
			: std::basic_ofstream<_Elem, _Traits>(a)
		{}


		std::basic_string<_Elem, _Traits> getColour(MB::RBColour colour) const
		{
			return colour == MB::RBColour::Red ? "darksalmon" : "gray50";
		}

		template <typename T>
		void outDirection(T const *source,  T const *target)
		{
			assert(source != nullptr);

			(*this) << source->data.payload << " [color=" << getColour(source->data.colour) << " style=filled]" << std::endl;

			if (target != nullptr)
			{
				(*this) << source->data.payload << " -> " << target->data.payload << ";" << std::endl;
			}
			else
			{
				// get unique null node id
				auto id = null_node_counter++;

				// hide duplicate null node
				(*this) << "null_" << id << " [label = \"null\" fillcolor=" << getColour(MB::RBColour::Black) << " shape=box style=filled width=0.3 height=0.16 fontsize=9]" << std::endl;

				(*this) << source->data.payload << " -> " << "null_" << id << std::endl;
			}
		}
	};

	using BinaryTreeVizStream = BasicBinaryTreeVizStream<char, std::char_traits<char>>;
	using RBTreeVizStream = BasicRBTreeVizStream<char, std::char_traits<char>>;

	template <typename T, typename Tree> struct StreamTraits;

	template <typename T>
	struct StreamTraits<T, TreeNode<T>>
	{
		using Stream = BinaryTreeVizStream;
	};

	template <typename T>
	struct StreamTraits<T, RBNode<T>>
	{
		using Stream = RBTreeVizStream;
	};

	template <typename T>
	std::ostream& operator<< (std::ostream& os, RBContents<T> const& nodeData)
	{
		os << "{" << nodeData.payload << ", " << (nodeData.colour == MB::RBColour::Red ? "RBColour::Red" : "RBColour::Black") << "}";

		return os;
	}

	template <typename T>
	std::ostream& operator<< (std::ostream& os, RBNode<T> const* node)
	{
		displayNode(os, node);

		return os;
	}

	template <typename T>
	std::ostream& operator<< (std::ostream& os, TreeNode<T> const *node)
	{
		displayNode(os, node);

		return os;
	}

	template <typename Tnode>
	void displayNode(std::ostream& os, Tnode const* node)
	{
		if (node == nullptr)
			throw std::logic_error("whoops!");

		using value_type = typename Tnode::value_type;

		auto const	w = static_cast<Tnode const *>(getWest(node)),
					e = static_cast<Tnode const *>(getEast(node));

		using Stream_t = typename StreamTraits<value_type, Tnode>::Stream;
		if (auto woz = dynamic_cast<Stream_t *>(&os))
		{
			woz->outDirection(node, w);
			woz->outDirection(node, e);
		}
		else 
		{
			os << "(" << "node" << " > " << "w" << ")";
			os << "(" << "node" << " > " << "e" << ")";
		}

		if (w != nullptr)	os << w;
		if (e != nullptr)	os << e;
	}


	template <typename T, typename Tnode>
	std::ostream& operator<< (std::ostream& os, BinaryTree<T, Tnode> const& tree)
	{
		os << "digraph BST\n"
			<< "{\n";

		if (tree.getRoot())
			os << &*tree.getRoot();
		else
			os << "<empty>";

		os << "}\n";

		return os;
	}
}
