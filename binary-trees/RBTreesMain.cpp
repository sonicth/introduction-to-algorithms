#include "RedBlackTree.h"
#include "BinaryTreeUtils.h"
#include <cassert>

using namespace std;
using namespace MB;

std::ostream & operator<< (std::ostream& os, RotationDirection const &dir)
{
	os << (dir == RotationDirection::West ? "Left" : "Right");
	return os;
}


////////////////////////////////////////////////////////////////
// tests
////////////////////////////////////////////////////////////////


std::unique_ptr<RBTree<int>> createTestTree1()
{

	auto trb = make_unique<MB::RBTree<int>>();
	trb->insert({ 10, RBColour::Red });
	trb->insert({ 7, RBColour::Black });
	trb->insert({ 12, RBColour::Black });
	trb->insert({ 11, RBColour::Red });
	trb->insert({ 555, RBColour::Red });
	trb->insert({ 5, RBColour::Black });
	trb->insert({ 8, RBColour::Red });
	trb->insert({ 3, RBColour::Black });

	return trb;
}


void testRotation()
{

}

void testRBTrees1()
{
	using T = int;
	using Stream_t = typename MB::StreamTraits<int, RBNode<T>>::Stream;

	auto t1 = createTestTree1();
	{
		Stream_t os("tree1.txt");
		os << *t1;
	}

	vector<RBContents<int>> nodes;
	MB::BinaryTree<int>::walk(t1->getRoot(),
		[&nodes](RBContents<int> const& x) { 
			nodes.push_back(x); 
		});
	vector< RBContents<int>> nodeData = { { 3, RBColour::Black }, { 5, RBColour::Black }, { 7, RBColour::Black }, { 8, RBColour::Red }, { 10, RBColour::Red }, { 11, RBColour::Red }, { 12, RBColour::Black }, { 555, RBColour::Red } };
	assert(nodes == nodeData);
	cout << endl;

	// rotate left
	t1->rotate(RotationDirection::West);
	{
		Stream_t os("tree1_leftrotate1.txt");
		os << *t1;
	}

	auto t1_rotleft = createTestTree1();
	// rotate right
	t1_rotleft->rotate(RotationDirection::West, getWest(t1_rotleft->getRoot()));
	{
		Stream_t os("tree1_leftrotate2.txt");
		os << *t1_rotleft;
	}

	auto t1_rotright = createTestTree1();
	// rotate right
	t1_rotright->rotate(RotationDirection::East);
	{
		Stream_t os("tree1_rightrotate1.txt");
		os << *t1_rotright;
	}
}

int main()
{
	testRBTrees1();
	//testRotation();
	return 0;
}