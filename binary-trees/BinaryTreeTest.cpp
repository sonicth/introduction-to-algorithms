#include "BinaryTree.h"
#include "BinaryTreeUtils.h"

#define BOOST_TEST_MODULE BinaryTreeTestModule
#include <boost/test/included/unit_test.hpp>


using namespace std;
namespace bu = boost::unit_test;
using namespace MB;

struct BinaryTreeParams
{
	// arguments
	int ac;
	char** av;

	BinaryTreeParams()
		: ac{ bu::framework::master_test_suite().argc }
		, av{ bu::framework::master_test_suite().argv }
	{
		ti1.insert(42);;
		ti3 = ti1;
		ti3.insert(10);
		ti3.insert(20);
		ti3.insert(100);
		ti3.insert(1);

		// for deletion
		t4a.insert(15);

		t4a.insert(5);
		t4a.insert(16);

		t4a.insert(3);
		t4a.insert(12);
		t4a.insert(20);

		t4a.insert(10);
		t4a.insert(18);
		t4a.insert(23);

		t4a.insert(6);

		t4a.insert(7);

		t4input = t4a;
		t4input.insert(13);

		t4b.insert(15);
		t4b.insert(5);
		t4b.insert(3);
		t4b.insert(12);
		t4b.insert(20);
		t4b.insert(13);
		t4b.insert(10);
		t4b.insert(18);
		t4b.insert(23);
		t4b.insert(6);
		t4b.insert(7);

		t4c.insert(15);
		t4c.insert(6);
		t4c.insert(16);
		t4c.insert(3);
		t4c.insert(12);
		t4c.insert(20);
		t4c.insert(13);
		t4c.insert(10);
		t4c.insert(18);
		t4c.insert(23);
		t4c.insert(7);
	}

	MB::BinaryTree<int> t0{ };
	MB::BinaryTree<int> t1;
	MB::BinaryTree<int> t3;
	MB::BinaryTree<int> t4input, t4a, t4b, t4c;

	// integer trees
	MB::BinaryTree<int> ti1;
	MB::BinaryTree<int> ti3;
};

//BOOST_AUTO_TEST_CASE(BinaryTreeStaticTest)
//{
//	using bintree = MB::BinaryTree<int>;
//}

BOOST_FIXTURE_TEST_CASE(BinaryTreeBasicTest, BinaryTreeParams)
{
	BOOST_TEST(t0.size() == 0);
	BOOST_TEST(t0.height() == 0);

	t1.insert(42);;
	BOOST_TEST(t1.size() == 1);
	BOOST_TEST(t1.height() == 1);

	t3 = t1;
	t3.insert(10);
	t3.insert(100);
	BOOST_TEST(t3.size() == 3);
	BOOST_TEST(t3.height() == 2);
}


BOOST_FIXTURE_TEST_CASE(BinaryTreeOutout, BinaryTreeParams)
{
	t1.insert(42);;
	t3 = t1;
	t3.insert(10);
	t3.insert(20);

	BinaryTreeVizStream os("BinTree1.txt");
	os << t1 << endl;
	os << t3 << endl;

}

BOOST_FIXTURE_TEST_CASE(BinaryTreeWalk, BinaryTreeParams)
{
	t1.insert(42);;
	t3 = t1;
	t3.insert(10);
	t3.insert(20);
	t3.insert(100);
	t3.insert(1);
	BinaryTreeVizStream os("BinTreeWalked.txt");
	os << t3;
	using Node = MB::BinaryTree<int>::Node;

	// walk recursively
	std::vector<int> v1, v2;
	MB::BinaryTree<int>::walk(
		t3.getRoot(), 
		[&v1](Node const &x) { v1.push_back(x.getData()); });

	// walk iteratively
	auto visitor = [&v2](Node const *x) 
	{
		v2.push_back(x->getData());
	};
	walkIteratively(t3, visitor);

	// compare the two
	BOOST_TEST(v1 == v2);

}

BOOST_FIXTURE_TEST_CASE(BinaryTreeQuery, BinaryTreeParams)
{
	t1.insert(42);;
	t3 = t1;
	t3.insert(10);
	t3.insert(20);
	t3.insert(100);
	t3.insert(1);

	t3.insert(21);
	t3.insert(22);
	t3.insert(23);

	t3.insert(59);
	t3.insert(58);
	t3.insert(57);
	t3.insert(56);
	t3.insert(55);

	BinaryTreeVizStream("BinTreeQuery.txt") << t3;

	// search
	auto it = t3.find(1);
	BOOST_TEST(it != nullptr);
	//BOOST_TEST(it->getWest()->getData() == 20);

	auto it2 = t3.findIter(1);
	BOOST_TEST(it2 != nullptr);
	BOOST_TEST((it2 == it));
	//BOOST_TEST(it2->getWest()->getData() == 20);

	// min/max
	auto minval = BinaryTree<int>::treeMin(t3.getRoot())->getData();
	auto maxval = BinaryTree<int>::treeMax(t3.getRoot())->getData();
	BOOST_TEST(minval == 1);
	BOOST_TEST(maxval == 100);

	auto rootNext = getData(BinaryTree<int>::successor(t3.getRoot()));
	BOOST_TEST(rootNext == 55);

	auto y = t3.findIter(23);
	auto yNext = getData(BinaryTree<int>::successor(y));
	BOOST_TEST(yNext == 42);
}

BOOST_FIXTURE_TEST_CASE(BinaryTreeDeleteA, BinaryTreeParams)
{
	BinaryTreeVizStream("BinTreeDeleteInput.txt") << t4input;
	BinaryTreeVizStream("BinTreeDeleteA.txt") << t4a;

	auto toDelete = t4input.findIter(13);
	t4input.delete_(toDelete);

	BOOST_TEST(t4input == t4a);
}

BOOST_FIXTURE_TEST_CASE(BinaryTreeDeleteB, BinaryTreeParams)
{
	BinaryTreeVizStream("BinTreeDeleteB.txt") << t4b;

	auto toDelete = t4input.findIter(16);
	t4input.delete_(toDelete);

	BOOST_TEST(t4input == t4b);
}

BOOST_FIXTURE_TEST_CASE(BinaryTreeDeleteC, BinaryTreeParams)
{
	BinaryTreeVizStream("BinTreeDeleteC.txt") << t4c;

	auto toDelete = t4input.findIter(5);
	t4input.delete_(toDelete);
	//BinaryTreeVizStream("BinTreeDeleteInputDeletedC.txt") << t4input;

	BOOST_TEST(t4input == t4c);
}
