#pragma once

#include "BinaryTree.h"

namespace MB
{
	enum class RBColour { Red, Black };
	enum class RotationDirection { West, East };

	template <typename T>
	class RBTree;

	template <typename T>
	struct RBContents
	{
		T payload;
		RBColour colour;

		bool operator< (RBContents<T> const& other) { return payload < other.payload; }
	};


	template <typename T>
	struct RBNode
	{
		using Node = RBNode<T>;
		using Ptr = Node*;
		using value_type = T;
		using Data = RBContents<T>;

		Data data;
		Node *p = nullptr, *east = nullptr, *west = nullptr;

		~RBNode() { if (east) delete east; if (west) delete west; }

		void setParent(Node* p_) { p = p_; }
		//void setParent(std::unique_ptr<Node> p_) { p = p_.get(); }


		RBNode(T&& payload_, RBColour c = RBColour::Black)
			: data{ payload_, c } {}

		RBNode(Data const &data_) : data{ data_ } {}
	};

	template <typename T>
	typename RBColour & colour(RBNode<T> *n)
	{
		return n->data.colour;
	}

	template <typename T>
	typename RBNode<T>::Data const &getData(RBNode<T> const *n)
	{
		return n->data;
	}

	template <typename T>
	RBNode<T> const* getWest(RBNode<T> const* n)
	{
		return n->west;
	}

	template <typename T>
	RBNode<T> const* getEast(RBNode<T> const* n)
	{
		return n->east;
	}

	template <typename T>
	RBNode<T>* getWest(RBNode<T>* n)
	{
		return n->west;
	}

	template <typename T>
	RBNode<T>* getEast(RBNode<T>* n)
	{
		return n->east;
	}

	template <typename T>
	RBNode<T>* setWest(RBNode<T>* n, std::unique_ptr<RBNode<T>>&& newn)
	{
		if (n->west) delete n->west;
		n->west = newn.release();
		n->west->p = n;
		return n->west;
	}

	template <typename T>
	RBNode<T>* setEast(RBNode<T>* n, std::unique_ptr<RBNode<T>>&& newn)
	{
		if (n->east) delete n->east;
		n->east = newn.release();
		n->east->p = n;
		return n->east;
	}

	template <typename T>
	RBNode<T>* setWest(RBNode<T>* n, RBContents<T>&& data)
	{
		if (n->west) delete n->west;
		n->west = new RBNode<T>{ std::move(data) };
		n->west->p = n;
		return n->west;
	}

	template <typename T>
	RBNode<T>* setEast(RBNode<T>* n, RBContents<T> && data)
	{
		if (n->east) delete n->east;
		n->east = new RBNode<T>{ std::move(data) };
		n->east->p = n;
		return n->east;
	}

	template <typename T>
	bool operator== (RBContents<T> const& a, RBContents<T> const& b)
	{
		return a.payload == b.payload && a.colour == b.colour;
	}

	template <typename T>
	class RBTree
		: public BinaryTree<T, RBNode<T>>
	{
		using Node = typename RBNode<T>;
	public:
		RBTree(typename Node::Ptr root_) 
			: BinaryTree < T, RBNode<T>>(std::forward< Node::Ptr&& >(root_))
		{}	
		RBTree() = default;

		bool operator== (RBTree<T> const &other) const
		{
			return equal(getRoot(), other.getRoot());
		}

		bool operator!= (RBTree<T> const &other) const
		{
			return !(*this == other);
		}

		// utilities
		void rotate(RotationDirection dir, Node* top = nullptr );

		void insert(typename Node::Data &&value)
		{
			auto z = insertViolating(std::move(value));
			insertFixup(z);
		}

		Node * insertViolating(typename Node::Data&& value)
		{
			Node *site = nullptr, *next = getRoot();

			while (next != nullptr)
			{
				site = next;

				if (value < site->data)
					next = getWest(next);
				else
					next = getEast(next);
			}

			//TODO value.colour = RBColour::Red;

			if (site == nullptr) // empty tree
			{
				setRoot(make_unique<Node>(value));
				return getRoot();
			}
			else if (value < site->data)
				return setWest(site, move(value));
			else
				return setEast(site, move(value));
		}

		void insertFixup(Node *z)
		{
			// check that is inserted before it is called!
			assert(getRoot() != nullptr);

			if (z == nullptr)
				return;

			RBNode<T> *(*getSide)(RBNode<T>*n) ;
			RotationDirection dirFirst, dirSecond;

			while (z->p != nullptr 
				&& colour(z->p) == RBColour::Red
				&&  z->p->p != nullptr)
			{
				//assert(z->p);
				//assert(z->p->p);
				if (z->p == getWest(z->p->p))
				{
					getSide = getEast;
					dirFirst = RotationDirection::West;
					dirSecond = RotationDirection::East;
				} else {
					getSide = getWest;
					dirFirst = RotationDirection::East;
					dirSecond = RotationDirection::West;
				}

				auto y = getSide(z->p->p);

				if (colour(y) == RBColour::Red)
				{
					colour(z->p) = RBColour::Black;
					colour(y) = RBColour::Black;
					colour(z->p->p) = RBColour::Red;
					z = z->p->p;
				} else {
					if (z == getSide(z->p))
					{
						z = z->p;
						rotate(dirFirst, z);
					}
					//QUESTION do we need these tests:
					assert(z->p);
					assert(z->p->p);

					colour(z->p) = RBColour::Black;
					colour(z->p->p) = RBColour::Red;
					rotate(dirSecond, z->p->p);
				}
			}

			colour(getRoot()) = RBColour::Black;
		}


	};


	template<typename T>
	inline void RBTree<T>::rotate(RotationDirection dir, RBTree<T>::Node* x)
	{
		if (!x)
			x = getRoot();

		auto first = &RBNode<T>::east,
			second = &RBNode<T>::west;
		if (dir == RotationDirection::East)
			swap(first, second);

		if (x->*first == nullptr)
			throw std::runtime_error("east of x cannot be nullptr to rotate!");

		auto y = x->*first;
		x->*first = y->*second;
		y->*second = x;

		// reset parent
		if (x->*first != nullptr)
			(x->*first)->p = x;

		// reparent y
		if (x != getRoot())
		{
			if (x == x->p->*first)
				x->p->*first = y;
			else {
				assert(x == x->p->*second);
				x->p->*second = y;
			}

			y->p = x->p;
		}
		else
		{
			// root is now at y
			_root.release();

			_root.reset(y);
		}

		x->p = y;

	}
}
