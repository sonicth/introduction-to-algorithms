#include "matrix.hpp"
#include "vector.hpp"
#include <iostream>
#include <cassert>

namespace Cramer
{
	template <size_t N, typename T>
	Matrix<N, N, T> insertCol(Matrix<N, N, T> const &m, Vector<N, T> const &v, size_t col_idx)
	{
		auto out = m;
		for (auto ri = 0; ri < N; ++ri)
		{
			out[ri][col_idx] = v[ri];
		}
		return out;
	}

	template <size_t N, typename T>
	Vector<N, T> solve(Matrix<N, N, T> const &transform, Vector<N, T> const &transformed)
	{
		Vector<N, T> out;

		auto tr_det = determinant(transform);

		for (auto i = 0; i < N; ++i)
		{
			auto inserted = insertCol(transform, transformed, i);
			out[i] = determinant(inserted) / tr_det;
		}

		return out;
	}
}

using namespace std;
void testCramer()
{
	//Matrix<3, 3, float> m = std::array<float, 9>{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	constexpr double data[] = { 8, 7, 3, -5 };
	array<double, 2> av = { 38, -1 };
	
	Matrix<2, 2, double> m = data;
	Vector v(av);

	auto out = Cramer::solve(m, v);

	// check
	assert(out[0] == 3 && out[1] == 2);

	cout << "cramer solution: "<< out[0] <<", "<<out[1] << endl;
}
