#include <array>

template <size_t N, typename T = double>
class Vector
{
public:
	using element_type = T;
	using value_type = std::array<T, N>;

	constexpr Vector(std::array<T, N> const &a)
		: data{a}
	{
	}

	constexpr Vector() = default;

	constexpr T &operator[] (int i) { return data[i]; }
	constexpr T const &operator[] (int i) const { return data[i];	}

private:
	value_type data;
};