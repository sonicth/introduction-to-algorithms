#include "matrix.hpp"
#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <random>
#include <cassert>

using namespace std;

void testMatrix()
{
	Matrix<1, 1> md1;
	Matrix<2, 2> md2;
	Matrix<3, 3> md;
	Matrix<3, 3, float> mf;

	// identity
	setIdentity(md1);
	setIdentity(md2);
	setIdentity(md);
	setIdentity(mf);

	// determinant
	auto d1 = determinant(md1);
	auto d2 = determinant(md2);
	auto d3 = determinant(md);

	cerr << d1 << " " << d2 << " " << d3 << endl;
	assert(d1 == 1);
	assert(d2 == 1);
	assert(d3 == 1);
}


int main()
{
	//testMatrix();
	testCramer();

    return 0;
}
