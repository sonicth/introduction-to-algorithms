#pragma once
#include <array>
#include <initializer_list>

template <size_t Ncols, size_t Nrows, typename T = double>
class Matrix
{
public:
	using element_type = T;
	using row_type = std::array<element_type, Ncols>;
	using value_type = std::array<row_type, Nrows>;

	template <size_t As>
	constexpr Matrix(std::array<T, As> const &l)
	{
		static_assert(As == Ncols * Nrows, "initializer list size should match the number of elements in the matrix");

		auto li = l.begin();
		for (int ri = 0; ri < Nrows; ++ri)
			for (int ci = 0; ci < Nrows; ++ci)
				data[ri][ci] = *li++;
	}

	constexpr Matrix(T const in_data[])
	{
		//static_assert((sizeof(in_data) / sizeof(in_data[0])) == Ncols * Nrows, "array size should match the number of elements in the matrix");

		for (int ri = 0; ri < Nrows; ++ri)
			for (int ci = 0; ci < Nrows; ++ci)
				data[ri][ci] = in_data[ri*Ncols + ci];
	}

	constexpr Matrix()
	{}

	// Matrix(){}
	row_type &operator[] (int ri)
	{
		return data[ri];
	}

	row_type const &operator[] (int ri) const
	{
		return data[ri];
	}
private:
	value_type data;
};

template <typename T, size_t Ncols, size_t Nrows>
void setIdentity(Matrix<Ncols, Nrows, T> &m)
{
	for (int ri = 0; ri < Nrows; ++ri)
		for (int ci = 0; ci < Ncols; ++ci)
		{
			m[ri][ci] = T(ri == ci ? 1 : 0);
		}
}

template <typename T, size_t Ncols, size_t Nrows>
T determinant(Matrix<Ncols, Nrows, T> const &m)
{
	static_assert(false, "not yet implemented for these dimensions!");;
}

template <> inline
double determinant<double, 1, 1>(Matrix<1, 1, double> const &m)
{
	return m[0][0];
}

template <> inline
double determinant<double, 2, 2>(Matrix<2, 2, double> const &m)
{
	return m[0][0] * m[1][1] - m[0][1] * m[1][0];
}

template <> inline
double determinant<double, 3, 3>(Matrix<3, 3, double> const &m)
{
	return 
		m[0][0] * (m[1][1] * m[2][2] - m[1][2] * m[2][1]) +
		m[0][1] * (m[1][2] * m[2][0] - m[1][0] * m[2][2]) * (-1) +
		m[0][2] * (m[0][0]* m[1][1] - m[0][1] * m[1][0])
		;

}

// algorithms
void testCramer();
