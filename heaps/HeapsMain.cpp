
#include "HeapsLib.h"

using namespace std;



void testHeapify()
{
	MB::Heap p{16,14,10,15,10,9,5};


	for (auto it4a = p.parentIterator();
		!it4a.isDone();
		++it4a)
	{
		cout << "parent idx " << *it4a << endl;
	}
	for (auto it4a = p.reverseChildIterator();
		!it4a.isDone();
		++it4a)
	{
		cout << "child idx " << *it4a << endl;
	}

	//MB::Heap<int> hp616{ 23, 17, 14, 6, 13, 10, 1, 5, 7, 12 };
	MB::Heap<int> hp616{ 16, 4,10, 14,7,9,3, 2,8,1 };

	cerr << "heap is valid? " << MB::heapIsValid(hp616) << endl;
	MB::buildHeap(hp616);
	cerr << "heap is valid? " << MB::heapIsValid(hp616) << endl;
	cerr << hp616 << endl;
	function<bool(int,int)> minf = less_equal<int>();
	MB::buildHeap(hp616, minf);
	cerr << "heap (min) is valid? " << MB::heapIsValid(hp616, minf) << endl;
	cerr << hp616 << endl;



}

int main()
{
	testHeapify();
	return 0;
}