#pragma once

#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <random>
#include <functional>
// debugging
#include <cassert>

namespace MB
{
	class IIterator
	{
	public:
		virtual ~IIterator() = default;
		virtual IIterator& operator++() = 0;
		virtual bool isDone() const noexcept = 0;
	};

	template <typename T>
	class Heap
	{
		using A = std::vector<int>;
		A _data;
		size_t _size;
	public:
		using Height_t = size_t;
		using NodeIdx_t = int;
		constexpr static NodeIdx_t INVALID_IDX = -1;
		constexpr static T INVALID_VALUE = T(INVALID_IDX);

		
	public:

		Heap(std::initializer_list<T> l)
			: _data{ l }, _size{ _data.size() }
		{}
		size_t size() const noexcept { return _size; }
		void setSize(size_t newsize) noexcept { _size = newsize; }
		size_t containerLength() const noexcept { return _data.size(); }
		A& getContainer() noexcept { return _data; }

		/// given a node index, get tree height
		Height_t static _nodeHeight(const NodeIdx_t idx) noexcept
		{
			return static_cast<Height_t>(log2(idx + 1));
		}

		/// index of the first node at a given height
		constexpr NodeIdx_t static _firstNodeOfHeight(const Height_t idx) noexcept
		{
			return (1 << idx) - 1;
		}

		/// node offset at a given index at its height level
		NodeIdx_t static _offsetAtHeight(const NodeIdx_t idx) noexcept
		{
			auto height = _nodeHeight(idx);
			auto first_at_my_height = _firstNodeOfHeight(height);
			return idx - first_at_my_height;
		}

		std::tuple<NodeIdx_t, NodeIdx_t> static _childernIdxs(const NodeIdx_t idx) noexcept
		{
			auto height = _nodeHeight(idx);
			auto offset = _offsetAtHeight(idx);
			auto child_height = height + 1;
			auto child_height_idx = _firstNodeOfHeight(child_height);
			return std::make_tuple(
				child_height_idx + offset * 2,
				child_height_idx + offset * 2 + 1);
		}


		NodeIdx_t static _parentIdx(const NodeIdx_t idx) noexcept
		{
			assert(idx > 0 || !"no parents at (or above) root");
			if (!(idx > 0))
				return -1;

			auto height = _nodeHeight(idx);
			auto offset = _offsetAtHeight(idx);
			auto parent_height = height - 1;
			auto parent_height_idx = _firstNodeOfHeight(parent_height);
			return parent_height_idx + offset / 2;
		}

		Height_t height() const noexcept
		{
			if (size() == 0)
				return 0;

			return _nodeHeight(size() - 1);
		}

		T& operator[] (const size_t idx) noexcept
		{
			return _data[idx];
		}
		T const & operator[] (const size_t idx) const noexcept
		{
			return _data[idx];
		}

		class HeapIterator
			: IIterator
		{
			Heap &heap;
		protected:
			NodeIdx_t idx;
		public:
			HeapIterator(Heap &heap_)
				: heap{ heap_ }, idx{ 0 }
			{}

			virtual void advance() noexcept = 0;

			virtual IIterator& operator++() override
			{
				if (!isDone())
					advance();

				return *this;
			}

			T & operator*()
			{	return heap[idx];	}
			T const& operator*() const
			{	return heap[idx];	}

			NodeIdx_t getIdx() const noexcept
			{
				return idx;
			}
		};

		class ParentIterator
			: public HeapIterator
		{
			NodeIdx_t const last_parent_idx;
		public:
			ParentIterator(Heap & heap_)
				: HeapIterator{ heap_ }
				, last_parent_idx{ Heap::_parentIdx(heap_.size() - 1) }
			{
			}
			bool isDone() const noexcept override
			{
				return idx > last_parent_idx;
			}

			void advance() noexcept override
			{
				++idx;
			}
		};

		class ReverseParentIterator
			: public HeapIterator
		{
			NodeIdx_t const first_parent_idx;
		public:
			ReverseParentIterator(Heap& heap_)
				: HeapIterator{ heap_ }
				, first_parent_idx{ 0 }
			{
				idx = Heap::_parentIdx(heap_.size() - 1);
			}
			bool isDone() const noexcept override
			{
				return idx < first_parent_idx;
			}

			void advance() noexcept override
			{
				--idx;
			}
		};

		ParentIterator parentIterator()
		{
			return ParentIterator(*this);
		}

		ReverseParentIterator reverseParentIterator()
		{
			return ReverseParentIterator(*this);
		}

		//REFACTOR shared code with parent iterator
		class ReverseChildIterator
			: public HeapIterator
		{
			NodeIdx_t const first_child_idx;
		public:
			ReverseChildIterator(Heap & heap_)
				: HeapIterator{ heap_ }
				, first_child_idx{ 1 }
			{
				idx = heap_.size() - 1;
			}

			bool isDone() const noexcept override
			{
				return idx < first_child_idx;
			}

			void advance() noexcept override
			{
				--idx;
			}
		};

		ReverseChildIterator reverseChildIterator()
		{
			return ReverseChildIterator(*this);
		}

		bool isLeaf(const size_t idx) 
		{
			throw std::logic_error("not implemented");
			return false;
		}


		/////// index-based member functions
		T & parentAt(NodeIdx_t idx)
		{
			return (*this)[_parentIdx(idx)];
		}

		std::tuple<T, T> childrenAt(NodeIdx_t idx) const
		{
			auto child_idxs = _childernIdxs(idx);

			auto idx1 = std::get<0>(child_idxs),
				idx2 = std::get<1>(child_idxs);

			//if (idx2 > (size() - 1)) throw std::out_of_range("child index outside of range");
			auto max_idx = size() - 1;
			auto val1 = idx1 <= max_idx ? (*this)[idx1] : INVALID_VALUE,
				val2 = idx2 <= max_idx ? (*this)[idx2] : INVALID_VALUE;
			return make_tuple(val1, val2);
		}

		friend std::ostream& operator<<(std::ostream& os, Heap const& h)
		{
			auto size = h.size();
			os << "{Heap: ";
			for (auto i = 0; i < size; ++i)
			{
				os << h[i];
				if (i != size - 1)
					os << ", ";
			}
			os << "}";
			return os;
		}
	};


	template <typename T>
	bool heapIsValid(
		Heap<T> &heap,
		std::function<bool(T, T)> validate_f_default =
			[](T const& parent_value, T const& child_value)
			{ return parent_value >= child_value; }
	)
	{
		auto validate_f = [&validate_f_default](T parent, T child)
		{
			if (!validate_f_default(parent, child))
			{
				cerr << "**comparison condition failed, parent: " << parent << ", child: " << child << endl;
				return false;
			}
			else
				return true;
		};

		for (auto it = heap.parentIterator();
			!it.isDone();
			++it)
		{
			auto i = it.getIdx();

			T child1, child2;
			tie(child1, child2) = heap.childrenAt(i);
			if (child1 != Heap<T>::INVALID_VALUE && !validate_f(*it, child1)) return false;
			if (child2 != Heap<T>::INVALID_VALUE && !validate_f(*it, child2)) return false;
		}

		return true;
	}

	template <typename T>
	void heapifyBad(
		Heap<T> &heap,
		std::function<bool(T, T)> compare_f = greater<T>())
	{
		for (auto rit = heap.reverseChildIterator();
			!rit.isDone();
			++rit)
		{
			auto& parent_ref = heap.parentAt(rit.getIdx());
			if (!compare_f(parent_ref, *rit))
				swap(parent_ref, *rit);
		}
	}

	template <typename T>
	void heapifyAt(
		Heap<T>& heap,
		typename Heap<T>::NodeIdx_t idx,
		std::function<bool(T, T)> compare_f)
	{
		decltype(idx) l, r, largest = idx;

		do
		{
			// advance index to the largest element
			idx = largest;

			tie(l, r) = heap._childernIdxs(idx);

			if (l < heap.size() && !compare_f(heap[largest], heap[l]))
			{
				largest = l;
			}
			if (r < heap.size() && !compare_f(heap[largest], heap[r]))
			{
				largest = r;
			}

			if (largest != idx)
			{
				swap(heap[idx], heap[largest]);
			}
		} while (largest != idx);

	}

	template <typename T>
	void buildHeap(
		Heap<T>& heap,
		std::function<bool(T, T)> compare_f = greater_equal<T>())
	{
		//for (auto rit = heap.reverseParentIterator();
		for (auto rit = heap.parentIterator();
			!rit.isDone();
			++rit)
		{
			heapifyAt(heap, rit.getIdx(), compare_f);
			//cerr << "**heapified " << heap << endl;
		}
	}

	template <typename T>
	void heapSort(
		Heap<T>& heap,
		std::function<bool(T, T)> compare_f = greater_equal<T>())
	{
		buildHeap(heap, compare_f);

		auto n = heap.containerLength();
		for (auto i = n - 1; i >= 1; --i)
		{
			// move largest element to the end
			swap(heap[0], heap[i]);

			//cerr << "**heap before swapping " << heap << endl;
			// reduce heap size
			heap.setSize(heap.size() - 1);

			// heapify root
			heapifyAt(heap, 0, compare_f);
		}
	}

}
