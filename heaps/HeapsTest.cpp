#include "HeapsLib.h"

#define BOOST_TEST_MODULE HeapsTestModule
#include <boost/test/included/unit_test.hpp>


using namespace std;
namespace bu = boost::unit_test;


struct HeapsParams
{
	// arguments
	int ac;
	char** av;

	HeapsParams()
		: ac{ bu::framework::master_test_suite().argc }
		, av{ bu::framework::master_test_suite().argv }
	{
	}

	MB::Heap<int> hp0{ };
	MB::Heap<int> hp1{ 14882 };
	MB::Heap<int> hp2{ 14, 882 };
	MB::Heap<int> hp3{ 1, 2,3, 4,5 };
	MB::Heap<int> hp4{ 1, 2,3, 4,5,6,7, 8 };
	MB::Heap<int> hp_max1{ 16, 10, 14, 1, 9, 11, 12 };
	MB::Heap<int> hp616{ 23, 17, 14, 6, 13, 10, 1, 5, 7, 12 };
	MB::Heap<int> hp616f{ 23, 17, 14, 7, 13, 10, 1, 5, 6, 12};
	MB::Heap<int> hp62{ 16,4,10,14,7,9,3,2,8,1 };

};

BOOST_AUTO_TEST_CASE(HeapStaticTest)
{
	using heapi = MB::Heap<int>;

	BOOST_TEST(heapi::_firstNodeOfHeight(3) == 7);
	BOOST_TEST(heapi::_firstNodeOfHeight(2) == 3);
	BOOST_TEST(heapi::_firstNodeOfHeight(1) == 1);
	BOOST_TEST(heapi::_firstNodeOfHeight(0) == 0);

	BOOST_TEST(heapi::_nodeHeight(7) == 3);
	BOOST_TEST(heapi::_nodeHeight(8) == 3);
	BOOST_TEST(heapi::_nodeHeight(1) == 1);
	BOOST_TEST(heapi::_nodeHeight(2) == 1);
	BOOST_TEST(heapi::_nodeHeight(3) == 2);
	BOOST_TEST(heapi::_nodeHeight(0) == 0);

	BOOST_TEST(heapi::_offsetAtHeight(7) == 0);
	BOOST_TEST(heapi::_offsetAtHeight(8) == 1);
	BOOST_TEST(heapi::_offsetAtHeight(8) == 1);
	BOOST_TEST(heapi::_offsetAtHeight(3) == 0);
	BOOST_TEST(heapi::_offsetAtHeight(6) == 3);

	// get childern indices
	auto ch0 = heapi::_childernIdxs(0);
	BOOST_TEST(get<0>(ch0) == 1); BOOST_TEST(get<1>(ch0) == 2);
	auto ch1 = heapi::_childernIdxs(2);
	BOOST_TEST(get<0>(ch1) == 5); BOOST_TEST(get<1>(ch1) == 6);
	auto ch2 = heapi::_childernIdxs(5);
	BOOST_TEST(get<0>(ch2) == 11); BOOST_TEST(get<1>(ch2) == 12);

	// get parent indices
	BOOST_TEST(heapi::_parentIdx(1) == 0); BOOST_TEST(heapi::_parentIdx(2) == 0);
	BOOST_TEST(heapi::_parentIdx(5) == 2); BOOST_TEST(heapi::_parentIdx(6) == 2);
	BOOST_TEST(heapi::_parentIdx(11) == 5); BOOST_TEST(heapi::_parentIdx(12) == 5);


	//BOOST_TEST(hp4[7] == 8);
}

BOOST_FIXTURE_TEST_CASE(HeapPropertiesTest, HeapsParams)
{
	BOOST_TEST(hp0.size() == 0);
	BOOST_TEST(hp0.height() == 0);

	BOOST_TEST(hp1.size() == 1);
	BOOST_TEST(hp1.height() == 0);

	BOOST_TEST(hp2.size() == 2);
	BOOST_TEST(hp2.height() == 1);

	BOOST_TEST(hp3.size() == 5);
	BOOST_TEST(hp3.height() == 2);

	BOOST_TEST(hp4.size() == 8);
	BOOST_TEST(hp4.height() == 3);
	BOOST_TEST(hp4[7] == 8);

	auto childern4 = hp4.childrenAt(0);
	BOOST_TEST(std::get<0>(childern4) == 2);
	BOOST_TEST(std::get<1>(childern4) == 3);
	auto childern4a = hp4.childrenAt(2);
	BOOST_TEST(std::get<0>(childern4a) == 6);
	BOOST_TEST(std::get<1>(childern4a) == 7);
}

BOOST_FIXTURE_TEST_CASE(HeapifyValidityTest, HeapsParams)
{
	function<bool(int, int)> 
		validate_min_f = [](auto parent_value, auto child_value)
	{
		return parent_value <= child_value;
	},
		validate_max_f = [](auto parent_value, auto child_value)
	{
		return parent_value >= child_value;
	};
	BOOST_TEST(heapIsValid(hp4, validate_min_f) == true);
	BOOST_TEST(heapIsValid(hp_max1) == true);
	BOOST_TEST(heapIsValid(hp616) == false);
	BOOST_TEST(heapIsValid(hp616f) == true);
	
	// heapify!
	MB::buildHeap(hp616);
	BOOST_TEST(heapIsValid(hp616) == true);
}

BOOST_FIXTURE_TEST_CASE(HeapSortTest, HeapsParams)
{
	auto& c616 = hp616.getContainer();
	auto &c62 = hp62.getContainer();

	BOOST_TEST(std::is_sorted(begin(c616), end(c616)) == false);
	BOOST_TEST(std::is_sorted(begin(c62), end(c62)) == false);

	// sort
	heapSort(hp616);
	heapSort(hp62);

	// now heaps should be sorted!
	BOOST_TEST(std::is_sorted(begin(c616), end(c616)) == true);
	BOOST_TEST(std::is_sorted(begin(c62), end(c62)) == true);

}
