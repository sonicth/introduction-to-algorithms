
#include <memory>
#include <utility>

namespace MB {

	template <typename T>
	class List
	{
		struct Node
		{
			T payload;
			std::unique_ptr<Node> next;
		};

		std::unique_ptr<Node> root;

	public:

		template <typename TestFun>
		Node const *find(TestFun const &pred) const
		{
			auto curr = root.get();
			while (curr != nullptr)
			{
				if (pred(curr->payload))
					return curr;


				curr = curr ? curr->next.get() : nullptr;
			}

			return nullptr;
		}

		//TODO use std::forward
		void insert(T const &payload)
		{
			auto newNode = std::make_unique<Node>();
			newNode->payload = payload;
			newNode->next = std::move(root);
			root = std::move(newNode);
		}
	};
	
	enum class SlotStatus
	{
		TAKEN = 0,
		EMPTY,
		DELETED,
	};

	template <template <class, class> class M, class Tkey, class Tval>
	class Map
	{
	public:
		void insert(Tkey const &k, Tval const &v)
		{
			static_cast<M<Tkey, Tval> *>(this)->_insert(k, v);
		}
	};

	template <typename Tkey, typename Tval>
	class MapChaining : Map<MapChaining, Tkey, Tval>
	{
		using Chain = List<std::pair<Tkey, Tval>>;
		static constexpr int SIZE = 16;

		Chain table[SIZE];

	public:
		//using value_type = T;

		MapChaining() = default;

		size_t _insert(Tkey const &k, Tval const &v)
		{
			auto h = std::hash<Tkey>{}(k);
			auto idx = h % SIZE;

			table[idx].insert(std::make_pair(k, v));

			return idx;
		}
	};

	template <typename Tkey, typename Tval>
	class MapOpenAddr : Map<MapChaining, Tkey, Tval>
	{
		struct Slot
		{
			std::pair<Tkey, Tval> payload;
			SlotStatus status;
		};

		Slot *data = nullptr;

	public:
		//using value_type = T;

		MapOpenAddr(size_t size = 16)
			: data{ new Slot[size] }
		{
		}

		size_t _insert(Tkey const &k, Tval const &v)
		{
			throw std::logic_error("not implemented yet!");
			return -1;
		}

		~MapOpenAddr()
		{
			delete[] data;
		}
	};


}
