#pragma once 

#include <limits>
#include <vector>
#include <iostream>

struct CostLayout
{
    int price = std::numeric_limits<int>::min();
    std::vector<int> indices;

    // new flat laout
    CostLayout(int price_, int index)
        : price{price_}, indices{{index}}
    {}

    CostLayout() {}

    static CostLayout empty() 
    {
        CostLayout l;
        l.price = 0;
        return l;
    }

    friend std::ostream& operator<<(std::ostream& os, CostLayout const & dt);
};

CostLayout operator+ (CostLayout const &a, CostLayout const &b)
{
    CostLayout c;
    c.price = a.price+b.price;
    
    // concat vectors
    c.indices.reserve(a.indices.size() + b.indices.size());
    c.indices.insert(end(c.indices), begin(a.indices), end(a.indices));
    c.indices.insert(end(c.indices), begin(b.indices), end(b.indices));

    return c;
}

bool operator< (CostLayout const &a, CostLayout const &b)
{
    return a.price < b.price;
}

std::ostream& operator<<(std::ostream& os, CostLayout const & l)
{
    os << "{price: "<<l.price<<", items: ";
    for (auto &s:l.indices)
    {
        if (&s != &l.indices.front()) 
            os << ", ";

        os << s;
    }    
    os <<    "}";
    return os;   
}
