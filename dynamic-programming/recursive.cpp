// #include <bits/stdc++.h>
#include "CostLayout.h"
#include <algorithm>
#include <string>
#include <iostream>
#include <functional>
#include <vector>
#include <optional>
#include <unordered_set>
using namespace std;

class PriceFinder
{
    vector<int> const &costs;

    int maxPerSize(int currSize) const
    {
        if (currSize <= 0)
            return 0;

        if (currSize == 1)
            return costs[0];

        cout << ".";

        vector<int> currCosts(currSize);

        for (int si = currSize; si >= 1; --si )
        {
            // fill the cost permuations
            currCosts[si-1] = costs[si-1] + maxPerSize(currSize - si);
        }

        return *max_element(begin(currCosts), end(currCosts));
    }

    mutable unordered_map<int, CostLayout> sums;

    CostLayout maxPerSizeAmortised(int currSize) const
    {
        if (currSize <= 0)
            return CostLayout::empty();

        if (sums.count(currSize))
            return sums[currSize];

        cout << ".";

        CostLayout layout;

        for (int si = 1; si <= currSize; ++si )
        {
            layout = max(layout, CostLayout(costs[si-1], si) + maxPerSizeAmortised(currSize - si));
            
            // auto layoutRemaining = maxPerSizeAmortised(currSize - si);

            // auto currentPrice = costs[si-1] + layoutRemaining.price;
            // if (currentPrice > layout.price)
            // {
            //     layout.price = ;
            // }
            
        }

        // memoise!
        sums[currSize] = layout;

        return layout;
    }

public:
    template <typename Tcont>
    PriceFinder(Tcont &&costs_): costs{costs_} {}

    CostLayout getFor(int size) const { 
#ifdef RECURSIVE
        return CostLayout{ maxPerSize(size) }; 
#else   
        return maxPerSizeAmortised(size); 
#endif // RECURSIVE
    }
};



int main()
{
    vector<int> costs{1, 5, 8, 9, 10, 17, 17, 20, 24, 30};
    PriceFinder priceFinder(costs);

    for (
        int i = 1;
        i <= costs.size(); ++i
    )
    {
        cout << "cost for size"<< i<<": "<< priceFinder.getFor(i) <<endl;
    }

    // cout << "cost for 7 is " << priceFinder.getFor(7).price;

    return 0;
}
