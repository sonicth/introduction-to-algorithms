#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;


vector<int> maxPerSizeBottomUp(vector<int> const &costs, int size)
{
    vector<int> sumsArr(size+1, -1);
    sumsArr[0] = 0;


    for (int si = 1; si <= size; ++si)
    {
        int price = -100;
        for (int ci = 0; ci < si; ++ci)
        {
            price = max(price, costs[si-ci-1] + sumsArr[ci]);
        }

        sumsArr[si] = price;
    }

    return sumsArr;
}

int main()
{
    vector<int> costs{1, 5, 8, 9, 10, 17, 17, 20, 24, 30};

    auto results = maxPerSizeBottomUp(costs, 10);

    int i = 0;
    for (auto r:results)
    {
        cout << i++ << ": " << r << endl;
    }
}
